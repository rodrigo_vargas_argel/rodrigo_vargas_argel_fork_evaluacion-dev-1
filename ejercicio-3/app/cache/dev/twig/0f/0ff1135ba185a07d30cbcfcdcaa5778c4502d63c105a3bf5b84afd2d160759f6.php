<?php

/* ::base.html.twig */
class __TwigTemplate_c6c2f837df535f28f033fb0275689c55649889f05b97db29e6e1e0be20e7e3d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e2ab5739db515b0bd3fd3118476f513710e120087c664d11a1cacb9157dea0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e2ab5739db515b0bd3fd3118476f513710e120087c664d11a1cacb9157dea0a->enter($__internal_0e2ab5739db515b0bd3fd3118476f513710e120087c664d11a1cacb9157dea0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <h1>Evaluación desarrolladorrrr</h1>
        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 13
        echo "    </body>
</html>
";
        
        $__internal_0e2ab5739db515b0bd3fd3118476f513710e120087c664d11a1cacb9157dea0a->leave($__internal_0e2ab5739db515b0bd3fd3118476f513710e120087c664d11a1cacb9157dea0a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_53754a4daa79cc2f9f6da6e581286f45606f24a919ceb02ab0911d176d00adbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53754a4daa79cc2f9f6da6e581286f45606f24a919ceb02ab0911d176d00adbe->enter($__internal_53754a4daa79cc2f9f6da6e581286f45606f24a919ceb02ab0911d176d00adbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Evaluación desarrollador 2017";
        
        $__internal_53754a4daa79cc2f9f6da6e581286f45606f24a919ceb02ab0911d176d00adbe->leave($__internal_53754a4daa79cc2f9f6da6e581286f45606f24a919ceb02ab0911d176d00adbe_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_de02dc5a36d2e4f0bd201d7d6a1a68d1be4f1d2170d700aefb3ea165f04e0660 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de02dc5a36d2e4f0bd201d7d6a1a68d1be4f1d2170d700aefb3ea165f04e0660->enter($__internal_de02dc5a36d2e4f0bd201d7d6a1a68d1be4f1d2170d700aefb3ea165f04e0660_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_de02dc5a36d2e4f0bd201d7d6a1a68d1be4f1d2170d700aefb3ea165f04e0660->leave($__internal_de02dc5a36d2e4f0bd201d7d6a1a68d1be4f1d2170d700aefb3ea165f04e0660_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_b37a092a22dca53b2180977f2979655f94db19fb4c53d2dfd8a6102fc30d3d21 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b37a092a22dca53b2180977f2979655f94db19fb4c53d2dfd8a6102fc30d3d21->enter($__internal_b37a092a22dca53b2180977f2979655f94db19fb4c53d2dfd8a6102fc30d3d21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b37a092a22dca53b2180977f2979655f94db19fb4c53d2dfd8a6102fc30d3d21->leave($__internal_b37a092a22dca53b2180977f2979655f94db19fb4c53d2dfd8a6102fc30d3d21_prof);

    }

    // line 12
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1691470180443edb9f1bf1f958f9ce9db0959b9de5f1821d5188b9de2605c1b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1691470180443edb9f1bf1f958f9ce9db0959b9de5f1821d5188b9de2605c1b0->enter($__internal_1691470180443edb9f1bf1f958f9ce9db0959b9de5f1821d5188b9de2605c1b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_1691470180443edb9f1bf1f958f9ce9db0959b9de5f1821d5188b9de2605c1b0->leave($__internal_1691470180443edb9f1bf1f958f9ce9db0959b9de5f1821d5188b9de2605c1b0_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 12,  83 => 11,  72 => 6,  60 => 5,  51 => 13,  48 => 12,  46 => 11,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Evaluación desarrollador 2017{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        <h1>Evaluación desarrolladorrrr</h1>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "C:\\laragon\\www\\test\\ejercicio-3\\app/Resources\\views/base.html.twig");
    }
}
