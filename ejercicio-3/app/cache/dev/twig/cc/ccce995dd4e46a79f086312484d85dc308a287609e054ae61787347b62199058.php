<?php

/* base.html.twig */
class __TwigTemplate_5863f935b20213704914261156933bc7878cbaf568d7f1386ff01177cb6d7706 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3be26aeec633d06d3fd233157a31a7bdb54905d34e9155f6da03642e4787b5a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3be26aeec633d06d3fd233157a31a7bdb54905d34e9155f6da03642e4787b5a1->enter($__internal_3be26aeec633d06d3fd233157a31a7bdb54905d34e9155f6da03642e4787b5a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <h1>Evaluación desarrolladorrrr</h1>
        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 13
        echo "    </body>
</html>
";
        
        $__internal_3be26aeec633d06d3fd233157a31a7bdb54905d34e9155f6da03642e4787b5a1->leave($__internal_3be26aeec633d06d3fd233157a31a7bdb54905d34e9155f6da03642e4787b5a1_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_9515d1002e609ef171ea656cac97924729b3cd5443785a5f4ddfd95dcb3ae0ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9515d1002e609ef171ea656cac97924729b3cd5443785a5f4ddfd95dcb3ae0ab->enter($__internal_9515d1002e609ef171ea656cac97924729b3cd5443785a5f4ddfd95dcb3ae0ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Evaluación desarrollador 2017";
        
        $__internal_9515d1002e609ef171ea656cac97924729b3cd5443785a5f4ddfd95dcb3ae0ab->leave($__internal_9515d1002e609ef171ea656cac97924729b3cd5443785a5f4ddfd95dcb3ae0ab_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3414967a80fc5d45a5f34bde6abd7919771c94a4e61f73bbb666b7714b41b061 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3414967a80fc5d45a5f34bde6abd7919771c94a4e61f73bbb666b7714b41b061->enter($__internal_3414967a80fc5d45a5f34bde6abd7919771c94a4e61f73bbb666b7714b41b061_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3414967a80fc5d45a5f34bde6abd7919771c94a4e61f73bbb666b7714b41b061->leave($__internal_3414967a80fc5d45a5f34bde6abd7919771c94a4e61f73bbb666b7714b41b061_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_97d956144de6cf5589c285364284486e764b8ce5b934bce88cdba19d91cf44f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97d956144de6cf5589c285364284486e764b8ce5b934bce88cdba19d91cf44f9->enter($__internal_97d956144de6cf5589c285364284486e764b8ce5b934bce88cdba19d91cf44f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_97d956144de6cf5589c285364284486e764b8ce5b934bce88cdba19d91cf44f9->leave($__internal_97d956144de6cf5589c285364284486e764b8ce5b934bce88cdba19d91cf44f9_prof);

    }

    // line 12
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_16f92f75663467bf3ecc499cda1e87281a8f0f9161b6d0dd6d15da9b78e8bd47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16f92f75663467bf3ecc499cda1e87281a8f0f9161b6d0dd6d15da9b78e8bd47->enter($__internal_16f92f75663467bf3ecc499cda1e87281a8f0f9161b6d0dd6d15da9b78e8bd47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_16f92f75663467bf3ecc499cda1e87281a8f0f9161b6d0dd6d15da9b78e8bd47->leave($__internal_16f92f75663467bf3ecc499cda1e87281a8f0f9161b6d0dd6d15da9b78e8bd47_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 12,  83 => 11,  72 => 6,  60 => 5,  51 => 13,  48 => 12,  46 => 11,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Evaluación desarrollador 2017{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        <h1>Evaluación desarrolladorrrr</h1>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "C:\\laragon\\www\\test\\ejercicio-3\\app\\Resources\\views\\base.html.twig");
    }
}
