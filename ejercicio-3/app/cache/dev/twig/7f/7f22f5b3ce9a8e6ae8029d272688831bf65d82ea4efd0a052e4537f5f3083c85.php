<?php

/* AQCompraVentaBundle:Default:show.html.twig */
class __TwigTemplate_bcf64f58979df43471972f8b09a043625ce879d158b008867da135a66aaccd26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AQCompraVentaBundle:Default:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_74531bd53a63488e8b0fd8886fee2813e30aefd1e525f2ba202d8a23889c40c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74531bd53a63488e8b0fd8886fee2813e30aefd1e525f2ba202d8a23889c40c1->enter($__internal_74531bd53a63488e8b0fd8886fee2813e30aefd1e525f2ba202d8a23889c40c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AQCompraVentaBundle:Default:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_74531bd53a63488e8b0fd8886fee2813e30aefd1e525f2ba202d8a23889c40c1->leave($__internal_74531bd53a63488e8b0fd8886fee2813e30aefd1e525f2ba202d8a23889c40c1_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_3552e1b55c1a268c52be46159c9c4560fde988ede4b3bbe5d98c48aa49b88c53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3552e1b55c1a268c52be46159c9c4560fde988ede4b3bbe5d98c48aa49b88c53->enter($__internal_3552e1b55c1a268c52be46159c9c4560fde988ede4b3bbe5d98c48aa49b88c53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "
";
        
        $__internal_3552e1b55c1a268c52be46159c9c4560fde988ede4b3bbe5d98c48aa49b88c53->leave($__internal_3552e1b55c1a268c52be46159c9c4560fde988ede4b3bbe5d98c48aa49b88c53_prof);

    }

    public function getTemplateName()
    {
        return "AQCompraVentaBundle:Default:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block body %}

{% endblock body %}", "AQCompraVentaBundle:Default:show.html.twig", "C:\\laragon\\www\\test\\ejercicio-3\\src\\AQ\\CompraVentaBundle/Resources/views/Default/show.html.twig");
    }
}
