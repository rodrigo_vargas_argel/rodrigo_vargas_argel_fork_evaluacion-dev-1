<?php

/* AQCompraVentaBundle:Default:index.html.twig */
class __TwigTemplate_4c58bae6c7cf4e6d0a5e72abe2ba5b9f45cda29fcfa1028731e2e6f7beaeb70d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AQCompraVentaBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_027b4e5ed54c3c4a7b045066ed15eef1c5b8a6b5eddcbc6f6e41a96e53ab7073 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_027b4e5ed54c3c4a7b045066ed15eef1c5b8a6b5eddcbc6f6e41a96e53ab7073->enter($__internal_027b4e5ed54c3c4a7b045066ed15eef1c5b8a6b5eddcbc6f6e41a96e53ab7073_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AQCompraVentaBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_027b4e5ed54c3c4a7b045066ed15eef1c5b8a6b5eddcbc6f6e41a96e53ab7073->leave($__internal_027b4e5ed54c3c4a7b045066ed15eef1c5b8a6b5eddcbc6f6e41a96e53ab7073_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_3f3c7a83dd8b469850ea2ce16094569077a83cec5e7139bca32040431fbba879 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f3c7a83dd8b469850ea2ce16094569077a83cec5e7139bca32040431fbba879->enter($__internal_3f3c7a83dd8b469850ea2ce16094569077a83cec5e7139bca32040431fbba879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "  <h2>Vehículos disponibles</h2>
  <ul>
    <li>Acá tiene que ir el listado</li>
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["autos"] ?? $this->getContext($context, "autos")));
        foreach ($context['_seq'] as $context["_key"] => $context["auto"]) {
            // line 7
            echo "    <h3>Id:";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "id", array()), "html", null, true);
            echo " <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "id", array()), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "modelo", array()), "html", null, true);
            echo " </a> -- Precio: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["auto"], "valor", array()), "html", null, true);
            echo " - </h3>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "  </ul>
";
        
        $__internal_3f3c7a83dd8b469850ea2ce16094569077a83cec5e7139bca32040431fbba879->leave($__internal_3f3c7a83dd8b469850ea2ce16094569077a83cec5e7139bca32040431fbba879_prof);

    }

    public function getTemplateName()
    {
        return "AQCompraVentaBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 9,  49 => 7,  45 => 6,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block body %}
  <h2>Vehículos disponibles</h2>
  <ul>
    <li>Acá tiene que ir el listado</li>
    {% for auto in autos %}
    <h3>Id:{{ auto.id }} <a href=\"{{ auto.id }}\"> {{ auto.modelo }} </a> -- Precio: {{ auto.valor }} - </h3>
    {% endfor %}
  </ul>
{% endblock body %}", "AQCompraVentaBundle:Default:index.html.twig", "C:\\laragon\\www\\test\\ejercicio-3\\src\\AQ\\CompraVentaBundle/Resources/views/Default/index.html.twig");
    }
}
