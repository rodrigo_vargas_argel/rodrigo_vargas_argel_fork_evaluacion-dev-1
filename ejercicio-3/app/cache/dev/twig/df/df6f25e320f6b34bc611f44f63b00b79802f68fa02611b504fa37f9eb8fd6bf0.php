<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_d186f1f1f265d71f6901e066869713f3635b8ce3b6b2d7ce591b5ea8a9a84b7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_806901d761ad384c4c0cfa3f3df4b7185e5dae8026b2fc8b4b6753f28962eef7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_806901d761ad384c4c0cfa3f3df4b7185e5dae8026b2fc8b4b6753f28962eef7->enter($__internal_806901d761ad384c4c0cfa3f3df4b7185e5dae8026b2fc8b4b6753f28962eef7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_806901d761ad384c4c0cfa3f3df4b7185e5dae8026b2fc8b4b6753f28962eef7->leave($__internal_806901d761ad384c4c0cfa3f3df4b7185e5dae8026b2fc8b4b6753f28962eef7_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5960155542b020f3c1f2bd7aa18f56e9f1ae803d49ea5cab15204253344a56a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5960155542b020f3c1f2bd7aa18f56e9f1ae803d49ea5cab15204253344a56a7->enter($__internal_5960155542b020f3c1f2bd7aa18f56e9f1ae803d49ea5cab15204253344a56a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_5960155542b020f3c1f2bd7aa18f56e9f1ae803d49ea5cab15204253344a56a7->leave($__internal_5960155542b020f3c1f2bd7aa18f56e9f1ae803d49ea5cab15204253344a56a7_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_065c1a4348238dd0432df49ef62d02318c3325783db6998513b1498549851566 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_065c1a4348238dd0432df49ef62d02318c3325783db6998513b1498549851566->enter($__internal_065c1a4348238dd0432df49ef62d02318c3325783db6998513b1498549851566_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_065c1a4348238dd0432df49ef62d02318c3325783db6998513b1498549851566->leave($__internal_065c1a4348238dd0432df49ef62d02318c3325783db6998513b1498549851566_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_e9c236202a7cf2e965a65af6dedea3ba0716e076ced1802a8b16cf3019d619df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9c236202a7cf2e965a65af6dedea3ba0716e076ced1802a8b16cf3019d619df->enter($__internal_e9c236202a7cf2e965a65af6dedea3ba0716e076ced1802a8b16cf3019d619df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_e9c236202a7cf2e965a65af6dedea3ba0716e076ced1802a8b16cf3019d619df->leave($__internal_e9c236202a7cf2e965a65af6dedea3ba0716e076ced1802a8b16cf3019d619df_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'TwigBundle::layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include 'TwigBundle:Exception:exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "C:\\laragon\\www\\test\\ejercicio-3\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
