<?php

namespace AQ\CompraVentaBundle\Controller;
use AQ\CompraVentaBundle\Entity\Vehiculo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $em=$this->getDoctrine()->getManager();
        $autos=$em->getRepository('AQCompraVentaBundle:Vehiculo')->findAll();

       // echo 'AUTOSSS';
       return $this->render('AQCompraVentaBundle:Default:index.html.twig', array('autos'=>$autos));
    }
    
    /**
     * @Template()
     */
    public function showAction($id){
        $em=$this->getDoctrine()->getManager();
        $modelo=$em->getRepository('AQCompraVentaBundle:Marca')->find($id);
        echo $modelo->getNombre();
        echo '</br>';
        $autos=$em->getRepository('AQCompraVentaBundle:Vehiculo')->find($id);

        echo '</br>';
        echo $autos->getModelo();
        echo '</br>';
        echo $autos->getValor();
       // echo $autos->getMarca();

        return $this->render('AQCompraVentaBundle:Default:show.html.twig', array('autos'=>$autos));
        
    }
}
