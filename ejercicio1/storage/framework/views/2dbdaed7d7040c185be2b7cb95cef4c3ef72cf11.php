<?php $__env->startSection('title', 'Main page'); ?>

<?php $__env->startSection('content'); ?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-t-lg">
                    <h1>
                        Mensajes para <?php echo e($nombre); ?>

                    </h1>
                    <small>
                        Mensajes
                    </small>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-filter">
                        <thead>
                        <th>Remitente</th>
                        <th>Fecha</th>
                        <th>Titulo</th>
                        <th>Texto</th>
                        </thead>
                        <?php $__currentLoopData = $mensajes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $registro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($registro->remitente); ?></td>
                                <td><?php echo e($registro->fechacreacion); ?></td>
                                <td><?php echo e($registro->titulo); ?></td>
                                <td><?php echo e($registro->texto); ?></td>

                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>