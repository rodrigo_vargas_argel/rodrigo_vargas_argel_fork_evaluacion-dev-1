<?php $__env->startSection('title', 'Main page'); ?>

<?php $__env->startSection('content'); ?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-t-lg">
                    <h1>
                        usuarios
                    </h1>
                    <small>
                        usuarios
                    </small>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-filter">
                        <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Opciones</th>
                        </thead>
                        <?php $__currentLoopData = $usuario; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $registro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($registro->id); ?></td>
                                <td><?php echo e($registro->nombre); ?></td>
                                  <td>
                                      <a href="<?php echo e(URL::route('ver_mensajes',$registro->id)); ?>"> <button class="btn btn-warning">Ver Mensajes</button></a>
                                      <a href="<?php echo e(URL::route('mensaje_nuevo',$registro->id)); ?>"> <button class="btn btn-info">Nuevo Mensaje</button></a>
                                    <!--<a  class="ll btn btn-danger"  href=""  nombre="">Eliminar</a>-->
                                </td></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>