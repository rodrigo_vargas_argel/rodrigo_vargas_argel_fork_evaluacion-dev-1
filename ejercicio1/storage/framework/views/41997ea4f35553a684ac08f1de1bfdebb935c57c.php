<?php $__env->startSection('title', 'Main page'); ?>

<?php $__env->startSection('content'); ?>

    <h3>Mensajes</h3>


    <div class="col-lg-6  col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> Mensaje de <strong> <?php echo e($usuario_nombre); ?> </strong></h5>

            </div>
            <div class="ibox-content">

                <form class="form-horizontal" name="form1" method="post" action="<?php echo e(route('mensajes_store')); ?>">


                <p>Ingrese los datos requeridos.</p>
                <?php if(count($errors)>0): ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <div class="form-group"><label class="col-lg-2 control-label">Titulo</label>

                    <div class="col-lg-10">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="remitente" id="remitente" value="<?php echo e($usuario_id); ?>" class="form-control">
                        <input type="text" name="titulo" id="titulo" placeholder="Titulo del mensaje" class="form-control"> <span class="help-block m-b-none">Máximo 20 caracteres.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Destinatarios</label>
                    <div class="col-lg-10">
                    <select name="destinatario"  id="destinatario" class="form-control">
                        <?php $__currentLoopData = $usuario; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $registro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($registro->id); ?>"><?php echo e($registro->nombre); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    </div>
                </div>
                   <div class="form-group">
                       <label class="col-lg-2 control-label">Mensaje</label>
                       <div class="col-lg-10">
                           <textarea name="texto" id="texto" class="form-control" ></textarea>
                       </div>
                   </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-sm btn-white" type="submit">Guardar Mensaje</button>

                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>