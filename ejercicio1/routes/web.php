<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index')->name("main");

Route::get('/usuarios', 'UsuarioController@index')->name("usuarios");
Route::get('nuevo_mensaje/{id}', 'MensajeController@index')->name("mensaje_nuevo");
Route::post('mensajes_store', 'MensajeController@store')->name("mensajes_store");
Route::get('ver_mensajes/{id}', 'MensajeController@show')->name("ver_mensajes");