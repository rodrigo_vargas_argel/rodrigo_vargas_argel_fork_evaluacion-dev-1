@extends('layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center m-t-lg">
                            <h1>
                              Ejercicio 1 AQmarket
                            </h1>
                            <small>
                              ejercicio confeccionado por Rodrigo Vargas Argel
                            </small>
                        </div>
                    </div>
                </div>
            </div>
@endsection
