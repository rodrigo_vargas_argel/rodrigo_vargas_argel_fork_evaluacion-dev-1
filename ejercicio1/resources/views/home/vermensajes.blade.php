@extends('layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-t-lg">
                    <h1>
                        Mensajes para {{$nombre}}
                    </h1>
                    <small>
                        Mensajes
                    </small>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-filter">
                        <thead>
                        <th>Remitente</th>
                        <th>Fecha</th>
                        <th>Titulo</th>
                        <th>Texto</th>
                        </thead>
                        @foreach($mensajes as $registro)
                            <tr>
                                <td>{{$registro->remitente}}</td>
                                <td>{{$registro->fechacreacion}}</td>
                                <td>{{$registro->titulo}}</td>
                                <td>{{$registro->texto}}</td>

                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection