@extends('layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-t-lg">
                    <h1>
                        usuarios
                    </h1>
                    <small>
                        usuarios
                    </small>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-filter">
                        <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Opciones</th>
                        </thead>
                        @foreach($usuario as $registro)
                            <tr>
                                <td>{{$registro->id}}</td>
                                <td>{{$registro->nombre}}</td>
                                  <td>
                                      <a href="{{URL::route('ver_mensajes',$registro->id)}}"> <button class="btn btn-warning">Ver Mensajes</button></a>
                                      <a href="{{URL::route('mensaje_nuevo',$registro->id)}}"> <button class="btn btn-info">Nuevo Mensaje</button></a>
                                    <!--<a  class="ll btn btn-danger"  href=""  nombre="">Eliminar</a>-->
                                </td></td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection