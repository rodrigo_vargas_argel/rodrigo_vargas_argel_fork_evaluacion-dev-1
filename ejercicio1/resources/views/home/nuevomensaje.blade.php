@extends('layouts.app')

@section('title', 'Main page')

@section('content')

    <h3>Mensajes</h3>


    <div class="col-lg-6  col-sm-6 col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5> Mensaje de <strong> {{$usuario_nombre}} </strong></h5>

            </div>
            <div class="ibox-content">

                <form class="form-horizontal" name="form1" method="post" action="{{route('mensajes_store')}}">


                <p>Ingrese los datos requeridos.</p>
                @if (count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group"><label class="col-lg-2 control-label">Titulo</label>

                    <div class="col-lg-10">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="remitente" id="remitente" value="{{$usuario_id}}" class="form-control">
                        <input type="text" name="titulo" id="titulo" placeholder="Titulo del mensaje" class="form-control"> <span class="help-block m-b-none">Máximo 20 caracteres.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Destinatarios</label>
                    <div class="col-lg-10">
                    <select name="destinatario"  id="destinatario" class="form-control">
                        @foreach($usuario as $registro)
                            <option value="{{$registro->id}}">{{$registro->nombre}}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                   <div class="form-group">
                       <label class="col-lg-2 control-label">Mensaje</label>
                       <div class="col-lg-10">
                           <textarea name="texto" id="texto" class="form-control" ></textarea>
                       </div>
                   </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-sm btn-white" type="submit">Guardar Mensaje</button>

                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>


@endsection