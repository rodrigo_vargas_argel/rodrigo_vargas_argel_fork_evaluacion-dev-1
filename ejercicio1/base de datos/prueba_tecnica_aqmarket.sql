/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : prueba_tecnica_aqmarket

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-11-07 22:08:58
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `mensaje`
-- ----------------------------
DROP TABLE IF EXISTS `mensaje`;
CREATE TABLE `mensaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FechaCreacion` date DEFAULT NULL,
  `remitente` int(11) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `texto` text DEFAULT NULL,
  `destinatario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mensaje
-- ----------------------------
INSERT INTO `mensaje` VALUES ('1', '2017-11-07', '1', 'Prueba', 'Esta es Una Prueba', null);
INSERT INTO `mensaje` VALUES ('2', '2017-11-08', '2', 'ddd', 'ddsds', '1');
INSERT INTO `mensaje` VALUES ('3', '2017-11-08', '3', '', '', '1');
INSERT INTO `mensaje` VALUES ('4', '2017-11-08', '2', 'hola', 'hola martin', '1');
INSERT INTO `mensaje` VALUES ('5', '2017-11-08', '1', 'jkasds', 'klsdksdfjk jksdjfsdb \r\nklsdkjdfsjkdfjhjnjksdh jsdiopasjsadfs', '2');
INSERT INTO `mensaje` VALUES ('6', '2017-11-08', '2', 'ooooo', 'oo oasod i  iasdhiuodash jkashdkjash', '1');
INSERT INTO `mensaje` VALUES ('7', '2017-11-08', '3', 'dfsgdg', 'fdg 54564dfv df5vg4dvdefe', '1');
INSERT INTO `mensaje` VALUES ('8', '2017-11-08', '1', '444', 'hola muño jsa dajskdh jkasdjkas jkasdjkashdb jkasdhjkashdkasd', '3');

-- ----------------------------
-- Table structure for `mensajedestinatario`
-- ----------------------------
DROP TABLE IF EXISTS `mensajedestinatario`;
CREATE TABLE `mensajedestinatario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MensajeId` int(11) NOT NULL,
  `Destinatario` int(11) NOT NULL,
  `Leido` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mensajedestinatario
-- ----------------------------
INSERT INTO `mensajedestinatario` VALUES ('1', '1', '2', '0');
INSERT INTO `mensajedestinatario` VALUES ('2', '4', '1', '0');
INSERT INTO `mensajedestinatario` VALUES ('3', '5', '2', '0');
INSERT INTO `mensajedestinatario` VALUES ('4', '6', '1', '0');
INSERT INTO `mensajedestinatario` VALUES ('5', '7', '1', '0');
INSERT INTO `mensajedestinatario` VALUES ('6', '8', '3', '0');

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'Martin Antolinez');
INSERT INTO `usuario` VALUES ('2', 'Meriadoc Brandigamo');
INSERT INTO `usuario` VALUES ('3', 'Muño Gustioz');

-- ----------------------------
-- View structure for `view_mensajedestinatatrio`
-- ----------------------------
DROP VIEW IF EXISTS `view_mensajedestinatatrio`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mensajedestinatatrio` AS select `usuario`.`nombre` AS `destinatario`,`usuario`.`id` AS `usuario_id`,`mensajedestinatario`.`id` AS `id`,`mensaje`.`titulo` AS `titulo`,`mensaje`.`id` AS `id_mensaje` from ((`mensajedestinatario` join `usuario` on(`mensajedestinatario`.`Destinatario` = `usuario`.`id`)) join `mensaje` on(`mensajedestinatario`.`MensajeId` = `mensaje`.`id`));

-- ----------------------------
-- View structure for `view_mensajes`
-- ----------------------------
DROP VIEW IF EXISTS `view_mensajes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mensajes` AS select `usuario`.`nombre` AS `nombre`,`mensaje`.`id` AS `id`,`mensaje`.`FechaCreacion` AS `fechacreacion`,`mensaje`.`titulo` AS `titulo`,`mensaje`.`texto` AS `texto` from (`mensaje` join `usuario` on(`mensaje`.`remitente` = `usuario`.`id`));

-- ----------------------------
-- View structure for `view_msg`
-- ----------------------------
DROP VIEW IF EXISTS `view_msg`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_msg` AS select `view_mensajedestinatatrio`.`usuario_id` AS `destinatario_id`,`view_mensajedestinatatrio`.`destinatario` AS `destinatario`,`view_mensajes`.`nombre` AS `remitente`,`view_mensajes`.`titulo` AS `titulo`,`view_mensajes`.`fechacreacion` AS `fechacreacion`,`view_mensajes`.`texto` AS `texto` from (`view_mensajedestinatatrio` join `view_mensajes` on(`view_mensajedestinatatrio`.`id_mensaje` = `view_mensajes`.`id`));
