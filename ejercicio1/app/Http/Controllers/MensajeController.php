<?php

namespace App\Http\Controllers;

use App\mensaje;
use App\mensajedestinatario;
use Illuminate\Http\Request;
use App\usuario;
use Illuminate\Support\Facades\DB;


class MensajeController extends Controller
{
    public function index($id)
    {
        $usuario=usuario::find($id);
        $nombre=$usuario->nombre;
        return view('home/nuevomensaje',['usuario'=>usuario::query()->get(), 'usuario_id'=>$id,'usuario_nombre'=>$nombre]);//
    }

    public function store(Request $request)

    {
        $hoy = date('Y-m-d H:i:s');
       /* $request->validate([
            'titulo' =>'required|max:50',
            'destinatario' => 'required',
            'texto'=>'required',
        ]);*/

        $sql=new mensaje;
        $sql->FechaCreacion = $hoy;
        $sql->remitente=$request->get('remitente');
        $sql->titulo=$request->get('titulo');
        $sql->texto=$request->get('texto');
        $sql->destinatario=$request->get('destinatario');
        $sql->save();
        $LastInsertId = $sql->id;

        $rsql=new mensajedestinatario;
        $rsql->MensajeId=$LastInsertId;
        $rsql->Destinatario=$request->get('destinatario');
        $rsql->Leido=0;
        $rsql->save();



        return redirect(route('usuarios'));
    }
    public function show($id)

    {
        $mensajes=DB::table('view_msg')->where('destinatario_id', '=', $id)->get();
        $usuario=usuario::find($id);
        $nombre=$usuario->nombre;
        return view('home/vermensajes',['mensajes'=>$mensajes,'nombre'=>$nombre]);
    }

}
