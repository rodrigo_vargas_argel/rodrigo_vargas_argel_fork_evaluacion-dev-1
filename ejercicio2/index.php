<?php
require_once("perro.php");
require_once ("Canino.php");
require_once ("mamifero.php");
require_once("lobo.php");
require_once("tigre.php");
require_once("leon.php");
/*
Empezaremos definiendo la jerarquia de clases
*/

function comer(mamiferoClass $obj)
{
    $obj->comer();
}
function correr(mamiferoClass $obj)
{
    $obj->correr();
}

function dormir(mamiferoClass $obj)
{
    $obj->dormir();
}

function rugir(mamiferoClass $obj)
{
    $obj->rugir();
}

echo 'EL PERRO:  <br>';
$miperro = new perroClass;
$miperro->dueño="Rodrigo";
$miperro->nombre= "Bandido";
$miperro ->raza ="Pastor Aleman";
rugir($miperro);
comer($miperro);
dormir($miperro);
correr($miperro);

echo '<br><br><br>';

echo 'EL LOBO:  <br>';
$milobo = new LoboClass;
$milobo->especie="Ibericos";
rugir($milobo);
comer($milobo);
dormir($milobo);
correr($milobo);

echo '<br><br><br>';

echo 'EL TIGRE:  <br>';
$mitigre = new TigreClass;
$mitigre->ubicacion="Asia ";

comer($mitigre);
dormir($mitigre);
correr($mitigre);

echo '<br><br><br>';

echo 'EL Leon:  <br>';
$mileon = new LeonClass;
$mileon->ubicacion="Africa ";
rugir($mileon);
comer($mileon);
correr($mileon);

?>