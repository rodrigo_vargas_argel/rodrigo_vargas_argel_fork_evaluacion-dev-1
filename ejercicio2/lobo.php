<?php
require_once ("Canino.php");
class LoboClass extends CaninoClass {
    public $especie;
    public $numero;

    public function rugir()
    {
        echo 'Los Lobos:  '. $this->especie. ' Aullan siempre que hay luna llena <br>';
    }
    public function dormir()
    {
        echo 'Ellos siempre duermen en el bosque dentro de las cuevas <br>';
    }
    public function correr()
    {
        echo "corren muy Rapido para cazar a su preza <br>";
    }
    public function comer()
    {
        echo 'Se alimentan en grupo y cazan en equipo <br>';
    }

}
?>