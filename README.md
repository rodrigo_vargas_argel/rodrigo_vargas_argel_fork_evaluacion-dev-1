# evaluacion-dev-aq

## Instrucciones generales
- Hacer un fork del proyecto.
- Crear 3 carpetas para cada uno de los ejercicios
- Subir la actualización dentro del plazo de 3 horas.
- Generar un Pull Request a una rama con tu nombre completo (nombre-apellido)


## Ejercicio 1
Cree una aplicación en algún framework PHP que permita generar notificaciones que se despachan a usuarios dentro del sistema.
Cada usuario tiene su propio inbox, donde pueda ver el listado de mensajes que recibe, ver el detalle y responder. También debe poder crear un nuevo mensaje y seleccionar el/los destinatarios a los que quiere que le lleguen. La base de datos debe estar compuesta por al menos:

##### Usuario
- Id
- Nombre (varchar 200)

##### Mensaje
- Id 
- FechaCreacion (Datetime)
- Remitente (FK a Usuario)
- Titulo (varchar 100)
- Texto (Text)

##### MensajeDestinatario
- Id
- MensajeId (FK a Mensaje)
- Destinatario (FK a Usuario)
- Leido (boolean)



## Ejercicio 2
Se requiere crear una jerarquía de clases donde comience con el nivel más abstracto en una clase `Mamifero`, con métodos abstractos tales como comer(), dormir(), correr(), rugir() todas retornan un String.
- De la clase abstracta va a tener dos sub clases que **también son abstractas** `Canino` y `Felino`, y de ellas tendríamos 4 clases `Perro`, `Lobo`, `Leon` y `Tigre` respectivamente dos en cada abstracta.

- Implementar jerarquía donde esté presente el **polimorfismo**, todas las implementaciones concretas deben de tener diferentes comportamiento para los métodos abstractos y además diferentes atributos, como por ejemplo la clase Perro podría tener el atributo dueño (un string con el nombre del su amo), el perro es alimentado por su amo y come pellets y duerme en el jardín o dentro de la casa, los perros ladran (forma de rugir), los perros tienen un nombre de mascota y una raza canina etc...

- Mientras que los lobos duermen en los bosques, en cuevas, viven en grupos, para comer tienen que cazar en grupo y rugen aullando en luna llena, que se yo, podrían tener un atributo numeroDeCamada (int que contiene la cantidad de integrantes de su camada), un atributo especie de lobo (Ibérico, Americano, Asiático, etc) etc.

- Los leones por ejemplo tienen que rugir muy fuerte, capaz de oírse a más de 8km de distancia, cazan en manada, principalmente gacelas y ñues.

- Los tigres son más solitarios, cazan solos y son de Asia (región siberiana) y , habitan en manglares y bosques monzónicos, principal presa son los jabalíes y ciervos.
Ubicación o localidad podría ser un atributo de la clase abstracta Mamífero, lo mismo con el color y altura, largo, peso, etc.

- Implementar una clase Index con método run (estático), donde se muestre e implemente el funcionamiento y colaboración de los objetos de la jerarquía, al menos crear dos instancias por cada clase concreta.

El ejercicio deberá tener las siguientes clases:

1. Mamifero.php(abstract)
2. Canino.php(abstract)
3. Felino.php(abstract)
4. Lobo.php
5. Perro.php
6. Leon.php
7. Tigre.php
8. index.php(con el run implementado)


## Ejercicio 3
El proyecto en Symfony 2 simula una aplicación que permite listar y visualizar vehículos que se encuentran en una compraventa, los cuales tienen modelo, marca y precio.

## Configuración
- Archivo *parameters.yml.dist* contiene configuración inicial.

## Instrucciones
Se solicita realizar lo siguiente:

1. En la ruta `/autos/` listar **todos** los vehículos existentes en la aplicación.
  1. Crear _Repository_ para obtener el listado.
  2. Utilizar el controlador `DefaultController.php` del bundle `CompraVentaBundle`.
2. Permitir a través de un link visualizar un vehículo en particular. Se debe mostrar en la ruta `/autos/{idDelVehiculo}`
3. Crear el formulario respectivo para poder crear vehículos.
4. Considerar en el listado de vehículos un botón para poder eliminar y editar un vehículo.

## Resultado esperado
#### Listado de vehículos:
  ![Listado](ejercicio-3/listado.png)

#### Despliegue de vehículo:
  ![Detalle](ejercicio-3/despliegue.png)
